<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Tactician\Middleware;
use Symfony\Component\Workflow\Registry;
use SymfonyBro\TaskBundle\Exception\TaskException;
use SymfonyBro\TaskBundle\Model\TransitionAwareInterface;
use SymfonyBro\TaskBundle\Model\WorkflowAwareInterface;

class WorkflowMiddleware implements Middleware
{

    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * WorkflowMiddleware constructor.
     * @param Registry $registry
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Registry $registry, EntityManagerInterface $entityManager)
    {
        $this->registry = $registry;
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     * @throws \SymfonyBro\TaskBundle\Exception\TaskException
     */
    public function execute($command, callable $next)
    {
        $task = $command->getTask();

        if (!$command instanceof TransitionAwareInterface) {
            return $next($command);
        }

        $result = $next($command);

        if (empty($command->getTransition())) {
            return $result;
        }

        $workflowName = null;
        if ($command instanceof WorkflowAwareInterface) {
            $workflowName = $command->getWorkflowName();
        }

        try {
            $workflow = $this->registry->get($task, $workflowName);
            $workflow->apply($command->getTask(), $command->getTransition());
            $this->entityManager->persist($command->getTask());
        } catch (Exception $exception) {
            throw new TaskException($command->getTask(), $exception->getMessage(), $exception->getCode(), $exception);
        }

        return $result;
    }
}
