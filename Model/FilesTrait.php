<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

trait FilesTrait
{
    /**
     * @var FileInterface[]|ArrayCollection
     */
    protected $files;

    /**
     * @return FileInterface[]
     */
    public function getFiles(): array
    {
        return $this->files->toArray();
    }

    /**
     * @param FileInterface $file
     * @return $this
     */
    public function addFile(FileInterface $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    /**
     * @param FileInterface $file
     * @return $this
     */
    public function removeFile(FileInterface $file)
    {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @param FileInterface[] $files
     * @return $this
     */
    public function setFiles(array $files)
    {
        $this->files = new ArrayCollection($files);
        return $this;

    }
}