<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Twig;

use SymfonyBro\TaskBundle\Model\CommentAwareInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class CommentExtension extends Twig_Extension
{
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('comments', function (Twig_Environment $environment, CommentAwareInterface $subject, $template = '@SymfonyBroTask/Comment/list.html.twig') {
                return $this->comments($environment, $subject, $template);
            }, [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function comments(Twig_Environment $environment, CommentAwareInterface $subject, $template)
    {
        return $environment->render($template, [
            'subject' => $subject,
        ]);
    }
}
