<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class TaskListController extends Controller
{
    protected function doIndexAction(Request $request)
    {
        $form = $this->createListFilter();

        $filterBuilder = $this->createListQueryBuilder();

        if ($request->query->has($form->getName())) {
            // manually bind values from the request
            $form->submit($request->query->get($form->getName()));
            // build the query from the given form object
            $this->get('lexik_form_filter.query_builder_updater')
                ->addFilterConditions($form, $filterBuilder);
        }

        $paginator = $this->get('knp_paginator');
        $page = $this->getPaginationPage($request);
        $limit = $this->getPaginationLimit($form);
        $pagination = $paginator->paginate($filterBuilder, $page, $limit, $this->getPaginationOptions());

        return $this->render($this->getListTemplate(), [
            'form' => $form->createView(),
            'pagination' => $pagination,
        ]);
    }

    abstract protected function createListQueryBuilder(): QueryBuilder;

    abstract protected function createListFilter(): FormInterface;

    abstract protected function getListTemplate(): string ;

    /**
     * Current page for paginator
     *
     * @param Request $request
     * @return int
     */
    protected function getPaginationPage(Request $request): int
    {
        return $request->query->getInt('page', 1);
    }

    /**
     * Items limit for paginator
     *
     * @param FormInterface $form
     * @return int
     */
    protected function getPaginationLimit(FormInterface $form): int
    {
        $limit = $form->getData()['pageSize'] ?? 64;

        return $limit;
    }

    /**
     * Options for paginator
     *
     * @return array
     */
    protected function getPaginationOptions(): array
    {
        return [
            'defaultSortFieldName' => 'task.updatedAt',
            'defaultSortDirection' => 'desc'
        ];
    }

}
