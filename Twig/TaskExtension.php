<?php

namespace SymfonyBro\TaskBundle\Twig;

use SymfonyBro\TaskBundle\Model\TaskInterface;
use SymfonyBro\TaskBundle\Model\WorkflowColorProvider;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

/**
 * Class TaskExtension
 *
 * @package SymfonyBro\TaskBundle\Twig
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskExtension extends Twig_Extension
{

    /**
     * @var WorkflowColorProvider
     */
    private $colorProvider;

    public function __construct(WorkflowColorProvider $colorProvider)
    {
        $this->colorProvider = $colorProvider;
    }

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('get_color', function (TaskInterface $subject, $workFlowName = null) {
                return $this->colorProvider->getColor($subject, $workFlowName);
            }),
        ];
    }

    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('get_color', function (TaskInterface $subject, $workFlowName = null) {
                return $this->colorProvider->getColor($subject, $workFlowName);
            }),
        ];
    }
}
