<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;

interface TaskManagerInterface
{
    /**
     * @param $commentCommand
     */
    public function addComment($commentCommand);

    /**
     * @param TaskInterface $task
     */
    public function saveTask(TaskInterface $task);

    /**
     * @param TaskInterface $task
     */
    public function removeTask(TaskInterface $task);

}
