<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
namespace SymfonyBro\TaskBundle\Model;


interface CommentAwareInterface
{
    /**
     * @param CommentInterface $comment
     */
    public function addComment(CommentInterface $comment);

    /**
     * @return array
     */
    public function getComments(): array;

    /**
     * @return CommentInterface|null
     */
    public function getLastComment();
}
