<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Middleware;

use League\Tactician\Middleware;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use SymfonyBro\ErpCoreBundle\Model\EventAwareInterface;
use SymfonyBro\ErpCoreBundle\Model\PreEventAwareInterface;

class EventDispatcherMiddleware implements Middleware
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * EventDispatcherMiddleware constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        if ($command instanceof PreEventAwareInterface) {
            $this->eventDispatcher->dispatch($command->getPreEventName(), $command->getPreEvent());
        }
        $result = $next($command);
        if ($command instanceof EventAwareInterface) {
            $this->eventDispatcher->dispatch($command->getEventName(), $command->getEvent());
        }

        return $result;
    }
}
