<?php

namespace SymfonyBro\TaskBundle\Model;

use InvalidArgumentException;
use Symfony\Component\Workflow\Registry;

/**
 * Class WorkflowColorProvider
 *
 * @package SymfonyBro\TaskBundle\Model
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
final class WorkflowColorProvider
{
    private $colors = [];

    /**
     * @var Registry
     */
    private $registry;

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    public function add($workflowName, $supported, array $colors = [])
    {
        $this->colors[$supported][$workflowName] = $colors;
    }

    public function getColor($subject, $workflowName = null)
    {
        try {
            $workflow = $this->registry->get($subject, $workflowName);
        } catch (InvalidArgumentException $exception) {
            return [];
        }

        $marking = $workflow->getMarking($subject);
        $places = $marking->getPlaces();

        return array_intersect_key($this->colors[get_class($subject)]['workflow.'.$workflow->getName()], $places);
    }
}
