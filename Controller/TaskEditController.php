<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use SymfonyBro\TaskBundle\Entity\Task;
use SymfonyBro\TaskBundle\Model\TaskInterface;

abstract class TaskEditController extends Controller
{
    protected function doEditAction(Task $task, Request $request)
    {
        $form = $this->createTaskForm($task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $this->getDoctrine()
                ->getManager()
                ->transactional(function (EntityManagerInterface $em) use ($task) {
                    $changeSet = $this->calculateChangeset($em, $task);
                    $this->get('symfony_bro_task.model.task_manager')->saveTask($task);

                    if (!empty($changeSet)) {
                        $this->handleChangeSet($changeSet, $task);
                    }
                });

            $this->handleAfterEdit($task);

            return $this->redirect($this->getRedirectUrl($task));
        }

        return $this->render($this->getEditTemplate(), [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }
    abstract protected function createTaskForm(Task $task): FormInterface;

    abstract protected function getRedirectUrl(Task $task): string;

    abstract protected function getEditTemplate(): string;

    protected function handleChangeSet(array $changeSet, Task $task) {}

    protected function handleAfterEdit(TaskInterface $task) {}

    protected function calculateChangeset(EntityManagerInterface $em, Task $task)
    {
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSets();
        $changeSet = $uow->getEntityChangeSet($task);

        return $changeSet;
    }
}
