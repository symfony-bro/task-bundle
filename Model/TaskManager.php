<?php

namespace SymfonyBro\TaskBundle\Model;

use League\Tactician\CommandBus;
use SymfonyBro\TaskBundle\CommandBus\Command\AddReportCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\RemoveReportCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\RemoveTaskCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\SaveTaskCommand;

/**
 * Class TaskManager
 *
 * @package SymfonyBro\TaskBundle\Model
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class TaskManager implements TaskManagerInterface
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * TaskManager constructor.
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function addComment($commentCommand)
    {
        $this->bus->handle($commentCommand);
    }

    public function saveTask(TaskInterface $task)
    {
        $command = new SaveTaskCommand($task);
        $this->bus->handle($command);
    }

    public function removeTask(TaskInterface $task)
    {
        $command = new RemoveTaskCommand($task);
        $this->bus->handle($command);
    }
}
