<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SymfonyBro\TaskBundle\Entity\Task;

abstract class TaskCloneController extends Controller
{
    protected function doCloneAction(Task $task)
    {
        $newTask = clone $task;

        $this->getDoctrine()
            ->getManager()
            ->transactional(function () use ($task, $newTask) {
                $this->get('symfony_bro_task.model.task_manager')->saveTask($newTask);
                $this->handleClone($task, $newTask);
            });

        return $this->redirect($this->getRedirectUrl($newTask));
    }

    abstract protected function getRedirectUrl(Task $task): string;

    protected function handleClone(Task $sourceTask, Task $newTask) {}
}
