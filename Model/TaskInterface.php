<?php

namespace SymfonyBro\TaskBundle\Model;

use DateTimeInterface;

/**
 *
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface TaskInterface extends CommentAwareInterface
{
    public function getId();

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface;

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface;

    /**
     * @return string[]
     */
    public function getState(): array;

    /**
     * @param string[] $state
     */
    public function setState(array $state);
}
