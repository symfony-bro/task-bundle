<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Command;

use SymfonyBro\TaskBundle\Model\TransitionAwareInterface;

class TransitionCommand extends SimpleCommentCommand implements TransitionAwareInterface
{
    /**
     * @var string
     */
    private $transition;

    /**
     * @return null
     */
    public function getTransition()
    {
        return $this->transition;
    }

    /**
     * @param string $transition
     * @return $this
     */
    public function setTransition(string $transition = null)
    {
        $this->transition = $transition;
        return $this;
    }
}
