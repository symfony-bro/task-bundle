<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Tests\CommandBus\Handler;


use PHPUnit\Framework\TestCase;
use SymfonyBro\TaskBundle\CommandBus\Command\SimpleCommentCommand;
use SymfonyBro\TaskBundle\CommandBus\Handler\AbstractSimpleCommentHandler;
use SymfonyBro\TaskBundle\Entity\Comment;
use SymfonyBro\TaskBundle\Entity\Task;

class SimpleCommentHandlerTest extends TestCase
{
    public function testHandle()
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject|Task $task */
        $task = $this->getMockForAbstractClass(Task::class);
        $command = new SimpleCommentCommand($task, ['text']);

        $comment = $this->getMockBuilder(Comment::class)
            ->getMockForAbstractClass()
        ;

        /** @var \PHPUnit_Framework_MockObject_MockObject|AbstractSimpleCommentHandler $handler */
        $handler = $this->getMockForAbstractClass(AbstractSimpleCommentHandler::class);

        $handler->expects($this->once())
            ->method('buildComment')
            ->with(['text'])
            ->willReturn($comment);

        $handler->expects($this->once())
            ->method('saveTaskAndComment')
            ->with($task);

        $handler->handle($command);
    }
}
