<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


interface FilesAwareInterface
{
    /**
     * @return FileInterface[]
     */
    public function getFiles(): array;

    /**
     * @param FileInterface[] $files
     */
    public function setFiles(array $files);

    /**
     * @param FileInterface $file
     */
    public function addFile(FileInterface $file);

    /**
     * @param FileInterface $file
     */
    public function removeFile(FileInterface $file);
}