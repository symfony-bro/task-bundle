<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;

interface ReportAwareInterface
{
    /**
     * @param ReportInterface $report
     */
    public function setReport(ReportInterface $report = null);

    /**
     * @return ReportInterface|null
     */
    public function getReport();
}
