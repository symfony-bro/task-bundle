<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Handler;

use SymfonyBro\TaskBundle\CommandBus\Command\SimpleCommentCommand;
use SymfonyBro\TaskBundle\Model\CommentInterface;
use SymfonyBro\TaskBundle\Model\TaskInterface;

abstract class AbstractSimpleCommentHandler
{
    public function handle(SimpleCommentCommand $command)
    {
        $task = $command->getTask();
        $comment = $this->buildComment($command->getData());
        $task->addComment($comment);

        $this->saveTaskAndComment($task, $comment);
    }

    /**
     * @param $data
     * @return CommentInterface
     */
    abstract protected function buildComment($data = null): CommentInterface;

    /**
     * @param TaskInterface $task
     * @param CommentInterface $comment
     */
    abstract protected function saveTaskAndComment(TaskInterface $task, CommentInterface $comment);
}
