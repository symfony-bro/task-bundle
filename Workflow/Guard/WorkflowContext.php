<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 17.03.17
 * Time: 13:20
 */

namespace SymfonyBro\TaskBundle\Workflow\Guard;


use Symfony\Component\Workflow\Workflow;

class WorkflowContext
{
    /**
     * Workflow instance
     *
     * @var Workflow
     */
    private $workflow;

    /**
     * Workflow subject instance
     *
     * @var object
     */
    private $subject;

    /**
     * Subject id
     *
     * @var string|int
     */
    private $subjectId;

    /**
     * WorkflowContext constructor.
     *
     * @param Workflow   $workflow
     * @param object     $subject
     * @param string|int $subjectId
     */
    public function __construct(Workflow $workflow, $subject, $subjectId)
    {
        $this->workflow  = $workflow;
        $this->subject   = $subject;
        $this->subjectId = $subjectId;
    }

    /**
     * @return Workflow
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * @return object
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return int|string
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * Returns workflow logger context
     *
     * @return array
     */
    public function getLoggerContext()
    {
        return [
            'workflow' => $this->workflow->getName(),
            'class'    => get_class($this->subject),
            'id'       => $this->subjectId
        ];
    }
}
