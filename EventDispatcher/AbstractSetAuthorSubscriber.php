<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use SymfonyBro\ErpCoreBundle\Model\AuthorAwareInterface;
use SymfonyBro\ErpCoreBundle\Model\UserInterface;

abstract class AbstractSetAuthorSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            TaskEvents::TASK_COMMENTED => 'setCommentAuthor',
            TaskEvents::TASK_SAVED => 'setTaskAuthor'
        ];
    }

    public function setTaskAuthor(TaskEvent $event)
    {
        $task = $event->getTask();
        if (!$task instanceof AuthorAwareInterface || $task->getAuthor() instanceof UserInterface) {
            return;
        }

        if ($this->getAuthor() instanceof UserInterface) {
            $this->setAuthor($task, $this->getAuthor());
        }
    }

    /**
     * @return UserInterface|null
     */
    abstract public function getAuthor();

    /**
     * @param AuthorAwareInterface $object
     * @param UserInterface $author
     */
    abstract public function setAuthor(AuthorAwareInterface $object, UserInterface $author);

    public function setCommentAuthor(TaskEvent $event)
    {
        $task = $event->getTask();
        $comment = $task->getLastComment();
        if (!$comment instanceof AuthorAwareInterface || $comment->getAuthor() instanceof UserInterface) {
            return;
        }

        if ($this->getAuthor() instanceof UserInterface) {
            $this->setAuthor($comment, $this->getAuthor());
        }
    }

}
