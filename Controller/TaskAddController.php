<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use SymfonyBro\TaskBundle\Entity\Task;

abstract class TaskAddController extends Controller
{
    protected function doAddAction(Request $request)
    {
        $form = $this->createTaskForm($request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $this->getDoctrine()
                ->getManager()
                ->transactional(function () use ($task, $form) {
                    $this->get('symfony_bro_task.model.task_manager')
                        ->saveTask($task);

                    $this->handleAdd($task);
                });

            $this->handleAfterAdd($task);

            return $this->redirect($this->getRedirectUrl($task));
        }

        return $this->render($this->getEditTemplate(), [
            'form' => $form->createView(),
        ]);
    }

    abstract protected function createTaskForm(Request $request): FormInterface;

    abstract protected function getRedirectUrl(Task $task): string;

    abstract protected function getEditTemplate(): string ;

    protected function handleAdd(Task $task) {}

    protected function handleAfterAdd(Task $task) {}
}
