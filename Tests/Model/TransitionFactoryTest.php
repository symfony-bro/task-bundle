<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Tests\Model;

use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Workflow\Workflow;
use SymfonyBro\TaskBundle\Entity\Task;
use SymfonyBro\TaskBundle\Form\CommentType;
use SymfonyBro\TaskBundle\Model\TransitionFactory;

class TransitionFactoryTest extends TypeTestCase
{
    public function testCreateForm()
    {
        $task = $this->createMock(Task::class);
        $workflow = $this->createMock(Workflow::class);
        $workflow->method('getEnabledTransitions')
            ->with($task)
            ->willReturn([]);

        /** @var TransitionFactory|PHPUnit_Framework_MockObject_MockObject $factory */
        $factory = $this->getMockBuilder(TransitionFactory::class)
            ->setConstructorArgs([$this->factory, $workflow])
            ->getMockForAbstractClass();
        $factory->method('getFormType')
            ->willReturn(CommentType::class);

        $form = $factory->createForm($task);

        $this->assertEquals('comment', $form->getName());
    }
}
