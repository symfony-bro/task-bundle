<?php

namespace SymfonyBro\TaskBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use SymfonyBro\TaskBundle\Workflow\Guard\ExpressionGuard;

/**
 * Class AbstractWorkflowGuardPass
 *
 * @important Concrete implementations must be added to bundle with PassConfig::TYPE_OPTIMIZE
 *
 * @package SymfonyBro\TaskBundle\DependencyInjection\Compiler
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractWorkflowGuardPass implements CompilerPassInterface
{
    const DEFINITION_ID = 'symfony_bro_task.workflow_guard.expression_guard';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $workflows = $this->getWorkflows();

        $this->registerWorkflowsConfiguration($container, $workflows);
    }

    abstract protected function getWorkflows(): array;

    /**
     * Registers workflows configuration section
     *
     * @param ContainerBuilder $container container
     * @param array $workflowsConfig workflows config
     */
    private function registerWorkflowsConfiguration(ContainerBuilder $container, array $workflowsConfig)
    {
        $guardDefinition = null;
        foreach ($workflowsConfig as $workflowName => $workflowConfig) {
            if (!empty($workflowConfig['guard'])) {
                if (null === $guardDefinition) {
                    if (!$container->hasDefinition(self::DEFINITION_ID)) {
                        $guardDefinition = $this->setupGuardDefinition($container);
                    } else {
                        $guardDefinition = $container->getDefinition(self::DEFINITION_ID);
                    }
                }
                $this->registerGuardConfiguration($guardDefinition, $workflowName, $workflowConfig['guard']);
            }
        }
    }

    private function setupGuardDefinition(ContainerBuilder $container)
    {
        $securityExpressionLanguageDefinition = $container->findDefinition('security.expression_language');
        $workflowRegistryDefinition = $container->findDefinition('workflow.registry');
        $authorizationChecker = $container->findDefinition('security.authorization_checker');
        $tokenStorage = new Reference('security.token_storage');
        $rolesHierarchy = new Reference('security.role_hierarchy');

        $guardDefinition = new Definition(ExpressionGuard::class, [
            $securityExpressionLanguageDefinition,
            $workflowRegistryDefinition,
            $authorizationChecker,
            $tokenStorage,
            $rolesHierarchy
        ]);

        $guardDefinition->addTag('monolog.logger', array('channel' => 'workflow'));
        $guardDefinition->addMethodCall('setLogger', [new Reference('logger')]);

        $container->setDefinition('symfony_bro_task.workflow_guard.expression_guard', $guardDefinition);

        return $guardDefinition;
    }

    /**
     * Adjusts guard configurations
     *
     * @param Definition $guardDefinition
     * @param string $workflowName workflow name
     * @param array $guardConfig workflow trigger config
     */
    private function registerGuardConfiguration(Definition $guardDefinition, $workflowName, $guardConfig)
    {
        if (isset($guardConfig['expression'])) {
            // register workflow-level guard
            $eventName = sprintf('workflow.%s.guard', $workflowName);
            $this->registerGuardListener($guardDefinition, $eventName, $workflowName, $guardConfig['expression']);
        }

        foreach ($guardConfig['transitions'] as $transition => $transitionConfig) {
            // register transition-level guard
            $eventName = sprintf('workflow.%s.guard.%s', $workflowName, $transition);
            $this->registerGuardListener($guardDefinition, $eventName, $workflowName, $transitionConfig);
        }
    }

    /**
     * Registers expression listener for guard event
     *
     * @param Definition $guardDefinition guard service definition
     * @param string $eventName event name
     * @param string $workflowName workflow name
     * @param string $expression guard expression
     */
    private function registerGuardListener($guardDefinition, $eventName, $workflowName, $expression)
    {
        $guardDefinition->addMethodCall('registerGuardExpression', [$eventName, $workflowName, $expression]);
        $guardDefinition->addTag('kernel.event_listener', ['event' => $eventName, 'method' => 'guardTransition']);
    }
}
