<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Entity;

use DateTime;
use DateTimeInterface;
use SymfonyBro\TaskBundle\Model\CommentInterface;

abstract class Comment implements CommentInterface
{
    /**
     * @var DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $transition;

    /**
     * @var string
     */
    protected $text;

    public function __construct(string $transition = null)
    {
        $this->transition = $transition;
        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getTransition()
    {
        return $this->transition;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text = null)
    {
        $this->text = $text;
        return $this;
    }
}
