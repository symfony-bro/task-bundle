<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Exception;


use Exception;
use SymfonyBro\TaskBundle\Model\TaskInterface;
use Throwable;

class TaskException extends Exception
{
    /**
     * @var  TaskInterface
     */
    private $task;

    public function __construct(TaskInterface $task, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->task = $task;
    }

    /**
     * @return TaskInterface
     */
    public function getTask(): TaskInterface
    {
        return $this->task;
    }
}
