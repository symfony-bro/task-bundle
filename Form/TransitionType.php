<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class TransitionType extends AbstractType
{
    public function getParent()
    {
        return ChoiceType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['task', 'workflow',])
            ->setDefined('transitions_filters')
            ->setDefaults(['transitions_filters' => null, 'label' => false, 'expanded' => true,])
            ->setAllowedTypes('transitions_filters', ['callable', 'null', 'array'])
            ->setAllowedTypes('task', [TaskInterface::class])
            ->setAllowedTypes('workflow', [Workflow::class])
            ->setNormalizer('transitions_filters', function (Options $options, $value) {
                if (null !== $value && !\is_array($value)) {
                    return [$value];
                }

                return $value;
            })
            ->setNormalizer('choices', function (Options $options, $value) {
                /** @var Workflow $workflow */
                $workflow = $options['workflow'];
                /** @var TaskInterface $task */
                $task = $options['task'];

                $transitions = $workflow->getEnabledTransitions($task);

                $t = array_map(function (Transition $e) {
                    return $e->getName();
                }, $transitions);

                if (null !== $options['transitions_filters']) {
                    foreach ($options['transitions_filters'] as $filter) {
                        $t = $filter($t, $options);
                    }
                }

                $t = ['symfony_bro.transition.add_text' => ''] + array_combine($t, $t);

                return $t;
            })
        ;
    }
}
