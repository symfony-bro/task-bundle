<?php

namespace SymfonyBro\TaskBundle\Model;

use DateTimeInterface;

/**
 * Class CommentInterface
 *
 * @package SymfonyBro\TaskBundle\Model
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
interface CommentInterface
{
    public function getId();

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface;

    /**
     * @return string|null
     */
    public function getTransition();

    /**
     * @return string|null
     */
    public function getText();
}
