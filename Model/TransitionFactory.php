<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Workflow\Workflow;

abstract class TransitionFactory
{
    /**
     * @var Workflow
     */
    protected $workflow;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * TransitionFactory constructor.
     * @param FormFactoryInterface $formFactory
     * @param Workflow $workflow
     */
    public function __construct(FormFactoryInterface $formFactory, Workflow $workflow)
    {
        $this->formFactory = $formFactory;
        $this->workflow = $workflow;
    }

    /**
     * @param TaskInterface $task
     * @param array $data
     * @param array $options
     * @return \Symfony\Component\Form\FormInterface
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     */
    public function createForm(TaskInterface $task, array $data = [], array $options = [])
    {
        $default = [
            'task' => $task,
            'workflow' => $this->workflow
        ];
        $options = array_merge($options, $default);

        $builder = $this->formFactory
            ->createNamedBuilder('comment', $this->getFormType(), $data, $options);
        $this->onBuildForm($builder);

        return $builder->getForm();
    }

    public function onBuildForm(FormBuilderInterface $builder) {}

    /**
     * @return string
     */
    abstract protected function getFormType(): string;

    /**
     * @param TaskInterface $task
     * @param null $data
     * @return TransitionAwareInterface
     */
    abstract public function createCommand(TaskInterface $task, $data = null): TransitionAwareInterface;
}
