<?php

namespace SymfonyBro\TaskBundle\DependencyInjection\Compiler;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Workflow;
use Symfony\Component\Workflow\Transition;
use SymfonyBro\TaskBundle\Workflow\Guard\ExpressionGuard;

/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
abstract class AbstractRegisterWorkflowPass implements CompilerPassInterface
{
    private $workflowRegistry = 'workflow.registry';
    private $workflowColorProvider = 'symfony_bro_task.model.workflow_color_provider';
    private $workflowGuardId = 'symfony_bro_task.workflow_guard.expression_guard';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!class_exists(Workflow\Workflow::class)) {
            throw new LogicException('Workflow support cannot be enabled as the Workflow component is not installed.');
        }

        $workflows = $this->getWorkflows();

        $this->registerWorkflowConfiguration($workflows, $container);

    }

    abstract protected function getWorkflows(): array;

    private function registerWorkflowConfiguration(array $workflows, ContainerBuilder $container)
    {
        if (!$container->hasDefinition($this->workflowRegistry)) {
            $this->setupRegistry($container);
        }
        $registryDefinition = $container->findDefinition($this->workflowRegistry);
        $colorProviderDefinition = $container->findDefinition($this->workflowColorProvider);

        foreach ($workflows as $name => $workflow) {
            $type = $workflow['type'];

            $transitions = array();
            $guardDefinition = null;
            foreach ($workflow['transitions'] as $transition_name => $transition) {
                if ($type === 'workflow') {
                    $transitions[] = new Definition(Transition::class, array($transition_name, $transition['from'], $transition['to']));
                } elseif ($type === 'state_machine') {
                    foreach ($transition['from'] as $from) {
                        foreach ($transition['to'] as $to) {
                            $transitions[] = new Definition(Transition::class, array($transition_name, $from, $to));
                        }
                    }
                }

                if (!empty($transition['guard'])) {
                    if (null === $guardDefinition) {
                        if (!$container->hasDefinition($this->workflowGuardId)) {
                            $guardDefinition = $this->setupGuardDefinition($container);
                        } else {
                            $guardDefinition = $container->getDefinition($this->workflowGuardId);
                        }
                    }
                    $this->registerGuardConfiguration($guardDefinition, $name, $transition_name, $transition['guard']);
                }
            }

            // Create a Definition
            $definitionDefinition = new Definition(Workflow\Definition::class);
            $definitionDefinition->setPublic(false);
            $definitionDefinition->addArgument($workflow['places']);
            $definitionDefinition->addArgument($transitions);
            $definitionDefinition->addTag('workflow.definition', array(
                'name' => $name,
                'type' => $type,
                'marking_store' => isset($workflow['marking_store']['type']) ? $workflow['marking_store']['type'] : null,
            ));
            if (isset($workflow['initial_place'])) {
                $definitionDefinition->addArgument($workflow['initial_place']);
            }

            // Create MarkingStore
            if (isset($workflow['marking_store']['type'])) {
                $markingStoreDefinition = new DefinitionDecorator('workflow.marking_store.' . $workflow['marking_store']['type']);
                foreach ($workflow['marking_store']['arguments'] as $argument) {
                    $markingStoreDefinition->addArgument($argument);
                }
            } elseif (isset($workflow['marking_store']['service'])) {
                $markingStoreDefinition = new Reference($workflow['marking_store']['service']);
            }

            // Create Workflow
            $workflowDefinition = new DefinitionDecorator(sprintf('%s.abstract', $type));
            $workflowDefinition->replaceArgument(0, $definitionDefinition);
            if (isset($markingStoreDefinition)) {
                $workflowDefinition->replaceArgument(1, $markingStoreDefinition);
            }
            $workflowDefinition->replaceArgument(3, $name);

            // Store to container
            $workflowId = sprintf('%s.%s', $type, $name);
            $container->setDefinition($workflowId, $workflowDefinition);
            $container->setDefinition(sprintf('%s.definition', $workflowId), $definitionDefinition);

            // Add workflow to Registry
            foreach ($workflow['supports'] as $supportedClass) {
                $registryDefinition->addMethodCall('add', array(new Reference($workflowId), $supportedClass));
                if (isset($workflow['colors'])) {
                    $colorProviderDefinition->addMethodCall('add', [$workflowId, $supportedClass, $workflow['colors']]);
                }
            }
        }
    }

    /**
     * @param $container
     */
    private function setupRegistry(ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__ . '/../../..') . '/Resources/config'));
        $loader->load('workflow.xml');
    }

    private function setupGuardDefinition(ContainerBuilder $container)
    {
        $securityExpressionLanguageDefinition = $container->findDefinition('security.expression_language');
        $workflowRegistryDefinition = $container->findDefinition('workflow.registry');
        $authorizationChecker = $container->findDefinition('security.authorization_checker');
        $tokenStorage = new Reference('security.token_storage');
        $rolesHierarchy = new Reference('security.role_hierarchy');

        $guardDefinition = new Definition(ExpressionGuard::class, [
            $securityExpressionLanguageDefinition,
            $workflowRegistryDefinition,
            $authorizationChecker,
            $tokenStorage,
            $rolesHierarchy
        ]);

        $guardDefinition->addTag('monolog.logger', array('channel' => 'workflow'));
        $guardDefinition->addMethodCall('setLogger', [new Reference('logger')]);

        $container->setDefinition('symfony_bro_task.workflow_guard.expression_guard', $guardDefinition);

        return $guardDefinition;
    }

    /**
     * Adjusts guard configurations
     *
     * @param Definition $guardDefinition
     * @param string $workflowName workflow name
     * @param array $guardConfig workflow trigger config
     */
    private function registerGuardConfiguration(Definition $guardDefinition, $workflowName, $transitionName, $guardExpression)
    {
        // register transition-level guard
        $eventName = sprintf('workflow.%s.guard.%s', $workflowName, $transitionName);
        $this->registerGuardListener($guardDefinition, $eventName, $workflowName, $guardExpression);
    }

    /**
     * Registers expression listener for guard event
     *
     * @param Definition $guardDefinition guard service definition
     * @param string $eventName event name
     * @param string $workflowName workflow name
     * @param string $expression guard expression
     */
    private function registerGuardListener($guardDefinition, $eventName, $workflowName, $expression)
    {
        $guardDefinition->addMethodCall('registerGuardExpression', [$eventName, $workflowName, $expression]);
        $guardDefinition->addTag('kernel.event_listener', ['event' => $eventName, 'method' => 'guardTransition']);
    }
}
