<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\EventDispatcher;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use SymfonyBro\ErpCoreBundle\Model\AuthorAwareInterface;
use SymfonyBro\ErpCoreBundle\Model\UserInterface;

/**
 * Class SetAuthorSubscriber
 * @package SymfonyBro\TaskBundle\EventDispatcher
 *
 * @TODO refactor to middlewares
 */
class SetAuthorSubscriber extends AbstractSetAuthorSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    public function getAuthor()
    {

        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            return null;
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return null;
        }

        return $user;
    }

    public function setAuthor(AuthorAwareInterface $object, UserInterface $author)
    {
        $object->setAuthor($author);
        $this->entityManager->persist($object);
        $this->entityManager->flush($object);
    }
}
