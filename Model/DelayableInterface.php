<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


use DateTime;

interface DelayableInterface
{
    /**
     * @param DateTime|null $delayedTo
     */
    public function setDelayedTo(DateTime $delayedTo = null);

    /**
     * @return DateTime|null
     */
    public function getDelayedTo();
}