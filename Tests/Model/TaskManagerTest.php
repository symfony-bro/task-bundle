<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Tests\Model;

use League\Tactician\CommandBus;
use PHPUnit\Framework\TestCase;
use SymfonyBro\TaskBundle\CommandBus\Command\AddReportCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\RemoveReportCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\RemoveTaskCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\SaveTaskCommand;
use SymfonyBro\TaskBundle\CommandBus\Command\SimpleCommentCommand;
use SymfonyBro\TaskBundle\Model\ReportAwareInterface;
use SymfonyBro\TaskBundle\Model\ReportInterface;
use SymfonyBro\TaskBundle\Model\TaskInterface;
use SymfonyBro\TaskBundle\Model\TaskManager;
use SymfonyBro\TaskBundle\Model\TaskManagerInterface;

class TaskManagerTest extends TestCase
{
    public function testSaveTask()
    {
        $bus = $this->getMockBuilder(CommandBus::class)
            ->disableOriginalConstructor()
            ->getMock();
        /** @var TaskInterface $task */
        $task = $this->getMockForAbstractClass(TaskInterface::class);

        /** @var TaskManagerInterface $manager */
        $manager = $this->getMockBuilder(TaskManager::class)
            ->setConstructorArgs([$bus])
            ->getMockForAbstractClass();

        $bus->expects($this->once())
            ->method('handle')
            ->with($this->callback(function ($subject) {
                return $subject instanceof SaveTaskCommand;
            }));

        $manager->saveTask($task);
    }

    public function testRemoveTask()
    {
        $bus = $this->getMockBuilder(CommandBus::class)
            ->disableOriginalConstructor()
            ->getMock();
        /** @var TaskInterface $task */
        $task = $this->getMockForAbstractClass(TaskInterface::class);

        /** @var TaskManagerInterface $manager */
        $manager = $this->getMockBuilder(TaskManager::class)
            ->setConstructorArgs([$bus])
            ->getMockForAbstractClass();

        $bus
            ->expects($this->once())
            ->method('handle')
            ->with($this->callback(function ($subject) {
                return $subject instanceof RemoveTaskCommand;
            }));

        $manager->removeTask($task);
    }

    public function testAddComment()
    {
        $bus = $this->getMockBuilder(CommandBus::class)
            ->disableOriginalConstructor()
            ->getMock();

        $command = $this->getMockBuilder(SimpleCommentCommand::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        /** @var TaskManagerInterface $manager */
        $manager = $this->getMockBuilder(TaskManager::class)
            ->setConstructorArgs([$bus])
            ->getMockForAbstractClass();

        $bus->expects($this->once())
            ->method('handle')
            ->with($this->callback(function ($subject) {
                return $subject instanceof SimpleCommentCommand;
            }));

        $manager->addComment($command);
    }
}
