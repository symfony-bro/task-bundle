<?php

namespace SymfonyBro\TaskBundle\Controller;

use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use SymfonyBro\TaskBundle\Entity\Task;
use SymfonyBro\TaskBundle\Model\TransitionFactory;

/**
 * Class CommentController
 * @package SymfonyBro\TaskBundle\Controller
 */
abstract class CommentController extends Controller
{
    /**
     * @param Task $task
     * @param Request $request
     * @param array $formOptions
     * @return Response
     * @throws InvalidOptionsException
     */
    protected function doFormAction(Task $task, Request $request, array $formOptions = []): Response
    {
        $addRoute = $this->getAddUrl($task);

        $data = $request->get('comment');
        $transition = $data['transition'] ?? null;

        $transitionFactory = $this->buildTransitionFactory($task, $transition);
        $form = $transitionFactory->createForm($task, $data ?? [], array_merge($formOptions, ['action' => $addRoute, 'attr' => ['autocomplete' => 'off'],]));

        return $this->renderFormAction([
            'form' => $form->createView(),
            'task' => $task,
            'add_url' => $addRoute,
            'form_url' => $this->getFormUrl($task),]
        );
    }

    /**
     * @param Task $task
     * @param Request $request
     * @param array $formOptions
     * @return Response
     * @throws InvalidArgumentException
     * @throws InvalidOptionsException
     */
    protected function doAddAction(Task $task, Request $request, array $formOptions = []): Response
    {
        $addRoute = $this->getAddUrl($task);

        $data = $request->get('comment');
        $transition = $data['transition'] ?? null;

        $transitionFactory = $this->buildTransitionFactory($task, $transition);


        $options = array_merge($formOptions, ['action' => $addRoute, 'attr' => ['autocomplete' => 'off'],]);
        $form = $transitionFactory->createForm($task, $data ?? [], $options);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $command = $transitionFactory->createCommand($task, $form);
                $command->setTransition($transition);

                $this->get('symfony_bro_task.model.task_manager')
                    ->addComment($command)
                ;

                return $this->renderValidAddAction(['task' => $task]);
            }

            return $this->renderInvalidAddAction($form);
        }

        return $this->renderAddAction([
            'form' => $form->createView(),
            'task' => $task,
            'add_url' => $addRoute,
            'form_url' => $this->getFormUrl($task),
        ]);
    }

    /**
     * @param Task $task
     * @param $transition
     * @return TransitionFactory
     */
    protected function buildTransitionFactory(Task $task, string $transition = null): TransitionFactory
    {
        $workflow = $this->get('workflow.registry')
            ->get($task)
        ;
        $builder = $this->creteFactoryBuilder($workflow->getName());

        return $builder->build($transition);
    }

    protected function creteFactoryBuilder(string $workflowName)
    {
        return $this->get('symfony_bro.task.factory_builder.'.$workflowName);
    }

    protected function renderInvalidAddAction(FormInterface $form): Response
    {
        return new Response(nl2br($form->getErrors(true)), 400);
    }

    abstract protected function renderValidAddAction(array $vars): Response;

    abstract protected function getAddUrl(Task $task): string;

    abstract protected function getFormUrl(Task $task): string;

    abstract protected function renderFormAction(array $vars): Response;

    abstract protected function renderAddAction(array $vars): Response;
}
