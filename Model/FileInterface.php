<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


use Symfony\Component\HttpFoundation\File\File;

interface FileInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getMimeType();

    /**
     * @return string
     */
    public function getOriginalName();

    /**
     * @return int
     */
    public function getSize();

    /**
     * @return File
     */
    public function getFile();
}