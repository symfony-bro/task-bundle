;(function ($, window, document, undefined) {
    "use strict";
    var pluginName = "symfonyBroComments",
        defaults = {
            alert: '#alert_placeholder',
            commentsForm: '#comments_form',
            additionalFields: '#additional_fields',
            addUrl: undefined,
            formUrl: undefined,
            onBeforeAdd: undefined,
            onAfterAdd: undefined,
            onBeforeChange: undefined,
            onChanged: undefined
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this.settings.transitionElementName = $(this.settings.transitions).find('input:first-child').attr("name");
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(Plugin.prototype, {
        init: function () {
            this.bindOnChangeTransition();
            var t = this;

            $(t.settings.commentsForm).on("submit", 'form', function (e) {
                e.preventDefault();
                var $form = $(e.target);
                //iphone hack
                var $inputs = $('input[type="file"]:not([disabled])', $form);
                $inputs.each(function (_, input) {
                    if (input.files.length > 0) {
                        return;
                    }
                    $(input).prop('disabled', true);
                });
                //
                var data = new FormData($form[0]);
                data.append($form.find("button[type=submit]:focus").attr('name'), '');
                if (t.settings.onBeforeAdd !== undefined) {
                    data = t.settings.onBeforeAdd.call(t, data);
                }

                $form.find(':input').prop("disabled", true);
                $.ajax({
                    url: t.settings.addUrl || $form.attr('action'),
                    type: "POST",
                    data: data,
                    processData: false,
                    contentType: false
                }).done(function (response) {
                    if (response.redirect_url) {
                        window.location.href = response.redirect_url;
                        return;
                    }
                    if (response.show_modal) {
                        $('#' + response.show_modal).modal('show');
                        return;
                    }

                    $(t.settings.commentsForm).replaceWith($(response).find(t.settings.commentsForm));
                    if (t.settings.onAfterAdd !== undefined) {
                        t.settings.onAfterAdd.call(t, response);
                    }
                    t.init();
                }).fail(function (response) {
                    if (200 !== response.status) {
                        t.showAlert(response.responseText.trim(), 'alert-danger');
                        return;
                    }
                    t.showAlert($(response.responseText).filter('title').text().trim() || response.statusText, 'alert-danger');
                }).always(function () {
                    $form.find(':input').prop("disabled", false);
                });

            });
        },
        bindOnChangeTransition: function () {
            var t = this;

            $(t.settings.commentsForm).on('change', t.settings.transitions, function () {
                var $form = $(t.settings.commentsForm).find('form');
                //iphone hack
                var $inputs = $('input[type="file"]:not([disabled])', $form);
                $inputs.each(function (_, input) {
                    if (input.files.length > 0) {
                        return;
                    }
                    $(input).prop('disabled', true);
                });
                //
                var formData = $form.serialize();
                var data = {};
                data[t.settings.transitionElementName] = $('[name="' + t.settings.transitionElementName + '"]:checked').val();

                if (t.settings.onBeforeChange !== undefined) {
                    data = t.settings.onBeforeChange.apply(t, [data, formData]);
                }

                $form.find(':input').prop("disabled", true);
                $.ajax({
                    url: t.settings.formUrl,
                    type: $form.attr('method'),
                    data: data
                }).done(function (response) {
                    $(t.settings.commentsForm).replaceWith($("<div>" + response + "</div>").find(t.settings.commentsForm));
                    if (t.settings.onChanged !== undefined) {
                        t.settings.onChanged.call(t, response);
                    }
                    t.init();
                }).fail(function (response) {
                    t.showAlert($(response.responseText).filter('title').text().trim() || response.statusText, 'alert-warning');
                }).always(function () {
                    $form.find(':input').prop("disabled", false);
                    t.deserialize($form, formData);
                });
            });
        },
        showAlert: function (message, alerttype) {
            $(this.settings.alert).append('<div id="alertdiv" class="alert ' + alerttype + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
        },
        deserialize: function (element, data, clearForm) {
            var splits = decodeURIComponent(data).split('&'),
                i = 0,
                split = null,
                key = null,
                value = null,
                splitParts = null;

            if (clearForm) {
                $('input[type="checkbox"],input[type="radio"]', element).removeAttr('checked');
                $('select,input[type="text"],input[type="password"],input[type="hidden"],textarea', element).val('');
            }

            var kv = {};
            while (split = splits[i++]) {
                splitParts = split.split('=');
                key = splitParts[0] || '';
                value = (splitParts[1] || '').replace(/\+/g, ' ');

                if (key != '') {
                    if (key in kv) {
                        if ($.type(kv[key]) !== 'array')
                            kv[key] = [kv[key]];

                        kv[key].push(value);
                    } else
                        kv[key] = value;
                }
            }

            for (key in kv) {
                value = kv[key];

                $('input[type="checkbox"][name="' + key + '"][value="' + value + '"],input[type="radio"][name="' + key + '"][value="' + value + '"]', element).prop('checked', true);
                $('select[name="' + key + '"],input[type="text"][name="' + key + '"],input[type="password"][name="' + key + '"],input[type="hidden"][name="' + key + '"],textarea[name="' + key + '"]', element).val(value);
            }
        }
    });

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" +
                    pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);
