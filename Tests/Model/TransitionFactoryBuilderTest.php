<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Tests\Model;


use LogicException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SymfonyBro\TaskBundle\Model\TransitionFactory;
use SymfonyBro\TaskBundle\Model\TransitionFactoryBuilder;

class TransitionFactoryBuilderTest extends TestCase
{
    public function testBuild()
    {
        $container = $this->createMock(ContainerInterface::class);
        $container
            ->expects($this->once())
            ->method('get')
            ->with("symfony_bro.task.transition_factory.wf.transition")
            ->willReturn($this->createMock(TransitionFactory::class));

        $container->method('has')
            ->willReturn(true);

        $builder = new TransitionFactoryBuilder($container, 'wf');

        $builder->build('transition');
    }

    public function testBuildException()
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->method('has')
            ->willReturn(false);

        $builder = new TransitionFactoryBuilder($container, 'wf');

        $this->expectException(LogicException::class);
        $builder->build('transition');
    }

    public function testBuildWithEmptyTransition()
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->method('has')
            ->willReturn(false);

        $builder = new TransitionFactoryBuilder($container, 'wf');

        $this->expectException(LogicException::class);
        $builder->build();
    }
}
