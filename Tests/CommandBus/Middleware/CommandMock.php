<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Tests\CommandBus\Middleware;


use SymfonyBro\TaskBundle\CommandBus\Command\SimpleCommentCommand;
use SymfonyBro\TaskBundle\Model\TransitionAwareInterface;

abstract class CommandMock extends SimpleCommentCommand implements TransitionAwareInterface
{

}
