<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\EventDispatcher;

use SymfonyBro\TaskBundle\Model\ReportAwareInterface;
use SymfonyBro\TaskBundle\Model\ReportInterface;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class TaskReportEvent extends TaskEvent
{
    /**
     * @var ReportInterface
     */
    private $report;

    /**
     * TaskReportEvent constructor.
     * @param ReportAwareInterface|TaskInterface $task
     * @param ReportInterface $report
     */
    public function __construct(ReportAwareInterface $task, ReportInterface $report = null)
    {
        parent::__construct($task);
        $this->report = $report;
    }

    /**
     * @return ReportInterface
     */
    public function getReport()
    {
        return $this->report;
    }
}
