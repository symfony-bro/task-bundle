<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\CommandBus\Command;


use Symfony\Component\EventDispatcher\Event;
use SymfonyBro\ErpCoreBundle\Model\EventAwareInterface;
use SymfonyBro\ErpCoreBundle\Model\PreEventAwareInterface;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvent;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvents;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class SaveTaskCommand implements EventAwareInterface, PreEventAwareInterface
{
    /**
     * @var TaskInterface
     */
    private $task;

    /**
     * SaveTaskCommand constructor.
     * @param TaskInterface $task
     */
    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }

    /**
     * @return TaskInterface
     */
    public function getTask(): TaskInterface
    {
        return $this->task;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return new TaskEvent($this->task);
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return TaskEvents::TASK_SAVED;
    }

    /**
     * @return Event
     */
    public function getPreEvent(): Event
    {
        return new TaskEvent($this->task);
    }

    /**
     * @return string
     */
    public function getPreEventName(): string
    {
        return TaskEvents::TASK_BEFORE_SAVE;
    }
}
