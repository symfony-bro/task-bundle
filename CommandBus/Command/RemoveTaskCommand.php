<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\CommandBus\Command;


use Symfony\Component\EventDispatcher\Event;
use SymfonyBro\ErpCoreBundle\Model\EventAwareInterface;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvent;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvents;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class RemoveTaskCommand implements EventAwareInterface
{
    /**
     * @var TaskInterface
     */
    private $task;

    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return new TaskEvent($this->task);
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return TaskEvents::TASK_REMOVED;
    }

    /**
     * @return TaskInterface
     */
    public function getTask(): TaskInterface
    {
        return $this->task;
    }
}
