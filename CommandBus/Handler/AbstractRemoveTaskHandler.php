<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Handler;

use SymfonyBro\TaskBundle\CommandBus\Command\RemoveTaskCommand;
use SymfonyBro\TaskBundle\Model\TaskInterface;

abstract class AbstractRemoveTaskHandler
{

    public function handle(RemoveTaskCommand $command)
    {
        $task = $command->getTask();
        $this->remove($task);
    }

    abstract protected function remove(TaskInterface $task);
}
