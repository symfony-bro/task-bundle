<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SymfonyBro\TaskBundle\Model\FileInterface;

/**
 * Class Image
 * @package SymfonyBro\TaskBundle\Entity
 */
abstract class File implements FileInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $mimeType;

    /**
     * @var string
     */
    protected $originalName;

    /**
     * @var integer
     */
    protected $size;

    /**
     * @var HttpFile
     */
    protected $file;

    /**
     * @var DateTimeImmutable
     */
    protected $updatedAt;

    public function __toString()
    {
        return (string)$this->originalName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     * @return $this
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param string $originalName
     * @return $this
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return HttpFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param HttpFile $file
     * @return $this
     */
    public function setFile(HttpFile $file)
    {
        $this->file = $file;

        $this->updatedAt = new DateTimeImmutable();

        if ($file instanceof UploadedFile) {
            $this->size = $file->getSize();
            $this->mimeType = $file->getMimeType();
            $this->originalName = $file->getClientOriginalName();
        }

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }


}
