<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SymfonyBro\TaskBundle\Entity\Task;

abstract class TaskViewController extends Controller
{
    protected function doViewAction(Task $task)
    {
        $this->handleView($task);

        return $this->render($this->getViewTemplate(), [
            'task' => $task,
        ]);
    }

    protected function handleView(Task $task)
    {
    }

    abstract protected function getViewTemplate(): string;
}
