<?php
/**
 * Created by PhpStorm.
 * User: pahhan.ne@gmail.com
 * Date: 17.03.17
 * Time: 13:17
 */

namespace SymfonyBro\TaskBundle\Workflow\Guard;

use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Exception\InvalidTokenConfigurationException;
use Symfony\Component\Workflow\Registry as WorkflowRegistry;
use Throwable;

class ExpressionGuard implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    /**
     * @var ExpressionLanguage
     */
    private $language;

    /**
     * @var WorkflowRegistry
     */
    private $workflowRegistry;

    /**
     * @var array
     */
    private $supportedEventsConfig = [];

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var RoleHierarchyInterface
     */
    private $roleHierarchy;

    /**
     * ExpressionGuard constructor
     *
     * @param ExpressionLanguage $language expression language
     * @param WorkflowRegistry $workflowRegistry workflow registry
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RoleHierarchyInterface $roleHierarchy
     */
    public function __construct(ExpressionLanguage $language, WorkflowRegistry $workflowRegistry, AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage, RoleHierarchyInterface $roleHierarchy)
    {
        $this->language = $language;
        $this->workflowRegistry = $workflowRegistry;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * Registers guard expression
     *
     * @param string $eventName guard event name
     * @param string $workflowName workflow name
     * @param string $expression guard expression
     */
    public function registerGuardExpression($eventName, $workflowName, $expression)
    {
        $this->supportedEventsConfig[$eventName] = [$workflowName, $expression];
    }

    /**
     * Blocks or allows workflow transitions by guard expression evaluation
     *
     * @param GuardEvent $event
     * @param string $eventName
     *
     * @throws \Exception in case of failure
     */
    public function guardTransition(GuardEvent $event, $eventName)
    {
        if (!array_key_exists($eventName, $this->supportedEventsConfig)) {
            throw new \LogicException(sprintf("Cannot find registered guard event by name '%s'", $eventName));
        }

        list($workflowName, $expression) = $this->supportedEventsConfig[$eventName];
        $subject = $event->getSubject();
        $workflowContext = new WorkflowContext(
            $this->workflowRegistry->get($subject, $workflowName),
            $subject,
            null
        );
        $loggerContext = $workflowContext->getLoggerContext();

        $expressionFailure = false;
        $errorMessage = null;

        try {
            $expressionResult = $this->language->evaluate($expression, $this->getVariables($event));
        } catch (Exception $e) {
            $errorMessage = sprintf(
                "Guard expression '%s' for guard event '%s' cannot be evaluated. Details: '%s'",
                $expression,
                $eventName,
                $e->getMessage()
            );
            $expressionFailure = true;
        } catch (Throwable $e) {
            $errorMessage = sprintf(
                "Guard expression '%s' for guard event '%s' cannot be evaluated. Details: '%s'",
                $expression,
                $eventName,
                $e->getMessage()
            );
            $expressionFailure = true;
        }

        if ($expressionFailure) {
            $this->logger->error($errorMessage, $loggerContext);

            return $event->setBlocked(true);
        }

        if (!is_bool($expressionResult)) {
            $this->logger->debug(
                sprintf("Guard expression '%s' for guard event '%s' evaluated with non-boolean result".
                    " and will be converted to boolean", $expression, $eventName),
                $loggerContext
            );
        }

        $event->setBlocked(!$expressionResult);

        if ($expressionResult) {
            $this->logger->debug(
                sprintf("Transition '%s' is blocked by guard expression '%s' for guard event '%s'",
                    $event->getTransition()->getName(),
                    $expression,
                    $eventName
                ),
                $loggerContext
            );
        }
    }

    // code should be sync with Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter
    private function getVariables(GuardEvent $event)
    {
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            throw new InvalidTokenConfigurationException(sprintf('There are no tokens available for workflow %s.', $event->getWorkflowName()));
        }

        if (null !== $this->roleHierarchy) {
            $roles = $this->roleHierarchy->getReachableRoles($token->getRoles());
        } else {
            $roles = $token->getRoles();
        }

        $variables = [
            'event' => $event,
            'token' => $token,
            'user' => $token->getUser(),
            'subject' => $event->getSubject(),
            'auth_checker' => $this->authorizationChecker,
        ];

        return $variables;
    }
}
