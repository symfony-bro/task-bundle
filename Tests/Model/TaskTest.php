<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Tests\Model;

use SymfonyBro\TaskBundle\Entity\Task;
use PHPUnit\Framework\TestCase;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class TaskTest extends TestCase
{
    public function testInterface()
    {
        /** @var Task $task */
        $task = $this->getMockBuilder(Task::class)->getMockForAbstractClass();

        $this->assertInstanceOf(TaskInterface::class, $task);
    }

    public function testConstructor()
    {
        /** @var Task $task */
        $task = $this->getMockBuilder(Task::class)
            ->disableProxyingToOriginalMethods()
            ->enableOriginalConstructor()
            ->getMockForAbstractClass()
        ;

        $this->assertInstanceOf(\DateTimeInterface::class, $task->getCreatedAt());
        $this->assertInstanceOf(\DateTimeInterface::class, $task->getUpdatedAt());
        $this->assertEquals([], $task->getState());
    }

}
