<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;

use LogicException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TransitionFactoryBuilder
{
    /**
     * @var ContainerInterface
     */
    private $container;

    private $workflowName;

    /**
     * FactoryBuilder constructor.
     * @param ContainerInterface $container
     * @param $workflowName
     */
    public function __construct(ContainerInterface $container, $workflowName)
    {
        $this->container = $container;
        $this->workflowName = $workflowName;
    }

    /**
     * @param string|null $transition
     * @return TransitionFactory
     * @throws LogicException
     */
    public function build(string $transition = null): TransitionFactory
    {
        $id = "symfony_bro.task.transition_factory.{$this->workflowName}.$transition";
        if(empty($transition)) {
            $id = "symfony_bro.task.transition_factory.{$this->workflowName}";
        }

        if (!$this->container->has($id)) {
            throw new LogicException("Factory service ($id) for transition $transition is not defined");
        }

        return $this->container->get($id);
    }
}
