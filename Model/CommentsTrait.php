<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

trait CommentsTrait
{
    /**
     * @var ArrayCollection|CommentInterface[]
     */
    protected $comments;

    /**
     * @return array
     */
    public function getComments(): array
    {
        return $this->comments->toArray();
    }

    /**
     * @param CommentInterface $comment
     * @return $this
     */
    public function addComment(CommentInterface $comment)
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
        }

        return $this;
    }

    /**
     * @return CommentInterface|null
     */
    public function getLastComment()
    {
        return $this->comments->last();
    }
}