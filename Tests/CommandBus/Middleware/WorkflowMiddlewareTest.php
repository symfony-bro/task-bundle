<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\Tests\CommandBus\Middleware;


use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;
use SymfonyBro\TaskBundle\CommandBus\Middleware\WorkflowMiddleware;
use SymfonyBro\TaskBundle\Entity\Task;

class WorkflowMiddlewareTest extends TestCase
{
    public function testApply()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $task = $this->createMock(Task::class);
        $workflow = $this->createMock(Workflow::class);
        $workflow
            ->expects($this->once())
            ->method('apply')
            ->with($task, 'transition');
        /** @var Registry|\PHPUnit_Framework_MockObject_MockObject $registry */
        $registry = $this->createMock(Registry::class);
        $registry->method('get')
            ->willReturn($workflow);

        $command = $this->createMock(CommandMock::class);
        $command->method('getTransition')
            ->willReturn('transition');
        $command->method('getTask')
            ->willReturn($task);
        $mw = new WorkflowMiddleware($registry, $em);
        $next = function ($arg) {
            return $arg;
        };

        $this->assertEquals($command, $mw->execute($command, $next));
    }
}
