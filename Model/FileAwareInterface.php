<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Model;


interface FileAwareInterface
{
    /**
     * @param FileInterface $file
     */
    public function setFile(FileInterface $file);

    /**
     * @return FileInterface
     */
    public function getFile();
}