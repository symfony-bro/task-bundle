<?php

namespace SymfonyBro\TaskBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use SymfonyBro\TaskBundle\DependencyInjection\Compiler\TransitionFactoryPass;

class SymfonyBroTaskBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new TransitionFactoryPass());
    }
}
