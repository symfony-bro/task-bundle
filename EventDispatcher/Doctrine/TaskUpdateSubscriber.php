<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\EventDispatcher\Doctrine;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\UnitOfWork;
use SymfonyBro\TaskBundle\Entity\Task;

class TaskUpdateSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }

    /**
     * @param OnFlushEventArgs $eventArgs
     *
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em  = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Task) {
                $this->notifyPropertyChanged($uow, $entity);
            }
        }

        /** @var PersistentCollection $col */
        foreach ($uow->getScheduledCollectionDeletions() as $col) {
            if ($col->getOwner() instanceof Task) {
                $this->notifyPropertyChanged($uow, $col->getOwner());
            }

        }
        /** @var PersistentCollection $col */
        foreach ($uow->getScheduledCollectionUpdates() as $col) {
            if ($col->getOwner() instanceof Task) {
                $this->notifyPropertyChanged($uow, $col->getOwner());
            }
        }
    }

    private function notifyPropertyChanged(UnitOfWork $uow, Task $entity)
    {
        $oldUpdatedAt = $entity->getUpdatedAt();
        $entity->touchUpdatedAt();
        $updatedAt = $entity->getUpdatedAt();
        $uow->propertyChanged($entity, 'updatedAt', $oldUpdatedAt, $updatedAt);
    }
}
