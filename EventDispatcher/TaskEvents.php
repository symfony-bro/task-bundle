<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
namespace SymfonyBro\TaskBundle\EventDispatcher;

final class TaskEvents
{
    const TASK_BEFORE_SAVE = 'symfony_bro.task.before_save';
    const TASK_SAVED = 'symfony_bro.task.task_saved';
    const TASK_COMMENTED = 'symfony_bro.task.task_commented';
    const TASK_REMOVED = 'symfony_bro.task.task_removed';
    const TASK_REPORT_SAVED = 'symfony_bro.task.task_report_saved';
}
