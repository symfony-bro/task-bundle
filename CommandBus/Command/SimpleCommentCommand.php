<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\CommandBus\Command;

use SymfonyBro\ErpCoreBundle\Model\EventAwareInterface;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvent;
use SymfonyBro\TaskBundle\EventDispatcher\TaskEvents;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class SimpleCommentCommand implements EventAwareInterface
{
    /**
     * @var TaskInterface
     */
    private $task;

    /**
     * @var mixed
     */
    private $data;

    /**
     * SaveTaskCommand constructor.
     *
     * @param TaskInterface $task
     * @param $data
     */
    public function __construct(TaskInterface $task, $data = null)
    {
        $this->task = $task;
        $this->data = $data;
    }

    /**
     * @return TaskInterface
     */
    public function getTask(): TaskInterface
    {
        return $this->task;
    }

    /**
     * @return TaskEvent
     */
    public function getEvent()
    {
        return new TaskEvent($this->task);
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return TaskEvents::TASK_COMMENTED;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
