<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\TaskBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SymfonyBro\TaskBundle\Entity\Task;

abstract class TaskRemoveController extends Controller
{
    protected function doRemoveAction(Task $task)
    {
        $this->getDoctrine()
            ->getManager()
            ->transactional(function () use ($task) {
                $this->get('symfony_bro_task.model.task_manager')
                    ->removeTask($task);

                $this->handleRemove($task);
            });

        return $this->redirect($this->getRedirectUrl($task));
    }

    abstract protected function getRedirectUrl(Task $task): string;

    protected function handleRemove(Task $task) {}
}
