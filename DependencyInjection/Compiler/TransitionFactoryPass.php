<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\TaskBundle\DependencyInjection\Compiler;


use LogicException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use SymfonyBro\TaskBundle\Model\TransitionFactoryBuilder;

class TransitionFactoryPass implements CompilerPassInterface
{
    private $factoryTag = 'symfony_bro.task.transition_factory';

    private $builderClass = TransitionFactoryBuilder::class;

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     * @throws \LogicException
     */
    public function process(ContainerBuilder $container)
    {
        $factories = $container->findTaggedServiceIds($this->factoryTag);

        foreach ($factories as $id => $tags) {
            foreach ($tags as $attributes) {
                if (!isset($attributes['workflow'])) {
                    throw new LogicException("'workflow' is required attribute for tag $this->factoryTag");
                }

                $workflow = $attributes['workflow'];

                $builderId = "symfony_bro.task.factory_builder.$workflow";
                if (!$container->hasDefinition($builderId)) {
                    $definition = new Definition($this->builderClass, [new Reference('service_container'), $workflow]);
                    $container->setDefinition($builderId, $definition);
                }

            }
        }
    }
}
