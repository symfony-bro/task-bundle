<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
namespace SymfonyBro\TaskBundle\CommandBus\Handler;

use SymfonyBro\TaskBundle\CommandBus\Command\SaveTaskCommand;
use SymfonyBro\TaskBundle\Model\TaskInterface;

abstract class AbstractSaveTaskHandler
{

    public function handle(SaveTaskCommand $command)
    {
        $task = $command->getTask();
        $this->save($task);
    }

    abstract protected function save(TaskInterface $task);
}
